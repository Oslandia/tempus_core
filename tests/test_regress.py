#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import sys
import os
import re
import unittest

from wpstempus.wps_client import *
from wpstempus.tempus_request import *

WPS_HOST = '127.0.0.1'
WPS_PATH = '/wps'


class TestRegress(unittest.TestCase):

    def setUp(self):
        self.client = HttpCgiConnection( WPS_HOST, WPS_PATH )
        self.wps = WPSClient(self.client)

    def test_no_roadmap( self ):
        tempus = TempusRequest( 'http://' + WPS_HOST + WPS_PATH )
        
        # same origin and destination
        ox=-1.548348
        oy=47.204879
        # must not crash
        tempus.request( plugin_name = 'sample_road_plugin',
                        origin = Point( ox, oy ),
                        steps = [ RequestStep(destination = Point(ox, oy)) ] )
                        
        # test no path
        try:
            tempus.request( plugin_name = 'sample_road_plugin',
                            origin = Point( -1.572348, 47.207937 ),
                            steps = [ RequestStep(destination = Point(-1.572309, 47.207423)) ] )
        except RuntimeError as e:
            pass

if __name__ == '__main__':
    unittest.main()



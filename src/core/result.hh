/**
 *   Copyright (C) 2012-2019 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPUS_RESULT_HH
#define TEMPUS_RESULT_HH

#include <vector>
#include "common.hh"
#include "cost.hh"
#include "restriction.hh"

namespace Tempus {

    struct Step {
        ///
        /// Internal step identifier
        int64_t id = 0;
        
        /// Identifier of the predecessor triplet in the result.
        /// Set to 0 if no predecessor exists.
        db_id_t pred_id = 0;

        /// Arc db identifier
        db_id_t arc_id = 0;

        /// Predecessor node id
        db_id_t pred_node_id = 0;

        /// Current node id
        db_id_t node_id = 0;

        /// Accumulated cost from the origin to reach the predecessor node
        float pred_cost = 0.0;

        /// Accumulated cost from the origin to reach the end node of the step
        float cost = 0.0;

        /// Transport mode taken on the arc
        db_id_t transport_mode_id = 0;
        
        /// Automaton state at the begining of the arc
        ArcRestrictionAutomaton::State automaton_state = 0;

        /// Of which trip this step is part of
        db_id_t pt_trip_id = 0;
        
        ///
        /// Wait time before departing from the origin of the step (in seconds)
        int32_t waiting_time = 0;
        
        ////
        /// Departure time
        int32_t departure_time = 0;
        
        ///
        /// Arrival time
        int32_t arrival_time = 0;
        
        /// 
        /// Cumulated travel time from the origin node, in seconds
        int32_t total_time = 0;
        
        ///
        /// Cumulated bicycle time from the origin node, in seconds
        int32_t total_bicycle_time = 0;
        
        ///
        /// Cumulated walking time from the origin node, in seconds
        int32_t total_walking_time = 0;
        
        ///
        /// Cumulated fare from the origin node
        float total_fare = 0;
        
        ///
        /// Number of mode changes from the origin node
        int16_t total_mode_changes = 0;
        
        ///
        /// Number of PT transfers from the origin node
        int16_t total_pt_transfers = 0;
        
        /// 
        /// True if the step belongs the a requested path 
        bool in_paths = false; 
    };
    
    typedef std::vector< Step > Steps;

}

#endif

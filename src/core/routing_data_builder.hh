/**
 *   Copyright (C) 2012-2015 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPUS_DATA_BUILDER_HH
#define TEMPUS_DATA_BUILDER_HH

#include "routing_data.hh"
#include "variant.hh"
#include "db.hh"

namespace Tempus
{

class RoutingDataBuilder
{
public:
    RoutingDataBuilder( const std::string& a_name ) : name_(a_name) {}
    virtual ~RoutingDataBuilder() {}

    /**
     * Import routing data from a PostgreSQL database
     */
    virtual std::unique_ptr<RoutingData> pg_import( const std::string& pg_options, const VariantMap& options = VariantMap() ) const;

    /**
     * Export routing data to a PostgreSQL database
     */
    virtual void pg_export( const RoutingData* rd, const std::string& pg_options, const VariantMap& options = VariantMap() ) const;

    /**
     * Import routing data from its serialized format in a file
     */
    virtual std::unique_ptr<RoutingData> file_import( const std::string& filename, const VariantMap& options = VariantMap() ) const;

    /**
     * Export routing data to its serialized format in a file
     */
    virtual void file_export( const RoutingData* rd, const std::string& filename, const VariantMap& options = VariantMap() ) const;

    /** Name (graph type) of this routing data builder */
    std::string name() const { return name_; }

    /** Version of the dump file format */
    virtual uint32_t version() const { return 1; }

    /** Write a serialization header to the given output stream */
    void write_header( std::ostream& ostr ) const;

    /** Read a serialization header from the given input stream.
     * Will throw on errors
     */
    void read_header( std::istream& istr ) const;

private:
    const std::string name_;
};

class RoutingDataBuilderRegistry
{
public:
    void addBuilder( std::unique_ptr<RoutingDataBuilder> builder );

    const RoutingDataBuilder* builder( const std::string& name );

    std::vector<std::string> builder_list() const;

    size_t builder_count() const { return builders_.size(); }

    static RoutingDataBuilderRegistry& instance();

private:
    RoutingDataBuilderRegistry() {}
    std::map<std::string, std::unique_ptr<RoutingDataBuilder>> builders_;
};

///
/// Method to load metadata from the db
void load_metadata( RoutingData& rd, Db::Connection& conn );

///
/// Method to load transport modes from the db
RoutingData::TransportModes load_transport_modes( Db::Connection& conn );

template <typename Record>
class RecordIterator
{
public:
    virtual size_t count() const = 0;
    virtual Optional<Record> value() = 0;
    virtual bool next() = 0;
};

RoutingData::TransportModes load_transport_modes( RecordIterator<TransportMode>& tm_iterator );

} // namespace Tempus

#endif

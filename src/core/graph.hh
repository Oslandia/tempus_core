/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2016-2019 CEREMA (http://www.cerema.fr)
 *   Copyright (C) 2012-2019 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPUS_GRAPH_HH
#define TEMPUS_GRAPH_HH

#ifdef _WIN32
#pragma warning(push, 0)
#endif
#include <boost/graph/compressed_sparse_row_graph.hpp>
#include <boost/graph/properties.hpp>
#ifdef _WIN32
#pragma warning(pop)
#endif
#include <unordered_map>

#include "point.hh"
#include "routing_data.hh"
#include "optional.hh"
#include "nonstd/variant.hpp"
#include "restriction.hh"
#include "cost.hh"

/**
   A Tempus::Graph is made of Tempus::Node and Tempus::Arc

   A Tempus::Node can be specialized into:
   - Tempus::PublicTransportStop

   A Tempus::Arc can be specialized into:
   - Tempus::RoadArc
   - Tempus::PublicTransportArc

   The graph is a graph::compressed_sparse_row_graph. It is then meant to be read only.
*/
namespace Tempus {

    /// Vertex index type. We are currently using a 64-bit integer, which means ~ 1e19 vertices in the graph
    typedef uint64_t VertexIndexType;
    /// Arc index type. We are currently using a 64-bit integer, which means ~ 1e19 arcs in the graph
    typedef uint64_t ArcIndexType;

    ///
    /// Used as Vertex
    /// Refers to the 'node' DB's table
    struct Node : public Base {
        /// coordinates
        DECLARE_RW_PROPERTY( coordinates, Point3D );
        
        /// BitField. Allowed parking vehicles
        DECLARE_RW_PROPERTY( vehicle_parking_rules, int32_t );
        
        /// Security time at this node, when boarding a new PT trip
        DECLARE_RW_PROPERTY( pt_boarding_security_time, int32_t );

        public:
            Node() : vehicle_parking_rules_(0), 
                     pt_boarding_security_time_(0)
            {}
            
            void add_vehicle_parking_rule ( db_id_t );
            void clear_vehicle_parking_rules ();
    };

    class Timetable
    {
        public:
            class TripTime
            {
            public:
                TripTime() {}
                TripTime( int32_t ldeparture_time, int32_t larrival_time, db_id_t lpt_trip_id, int32_t ltraffic_rules ) :
                    departure_time_( ldeparture_time ),
                    arrival_time_( larrival_time ),
                    pt_trip_id_( lpt_trip_id ), 
                    traffic_rules_( ltraffic_rules )
                {}
                ///
                /// departure time, in seconds since a time origin
                DECLARE_RW_PROPERTY( departure_time, int32_t );
                ///
                /// arrival time, in seconds since a time origin
                DECLARE_RW_PROPERTY( arrival_time, int32_t );
                ///
                /// trip id
                DECLARE_RW_PROPERTY( pt_trip_id, db_id_t );
                ///
                /// traffic rule
                DECLARE_RW_PROPERTY( traffic_rules, int32_t );
            };
            
            ///
            /// Unitary addition to the timetable.
            /// After all costs have been added, sort() must be called
            void add( const TripTime );

            ///
            /// Make sure the time table is sorted and a reverse time table exists
            void sort();

            using TripTimeIterator = std::vector<TripTime>::const_iterator;
            ///
            /// Get the next departure
            Optional<TripTime> next_departure( int32_t traffic_rule, int32_t time_min, Optional<db_id_t> pt_trip_id, int32_t boarding_security_time ) const;

            ///
            /// Get the previous arrival
            Optional<TripTime> previous_arrival( int32_t traffic_rule, int32_t time_min, Optional<db_id_t> pt_trip_id, int32_t boarding_security_time ) const;
        private:
            // vector of times, sorted by ascending departure_time
            std::vector<TripTime> table_ = std::vector<TripTime>();
            // vector of times, sorted by descending arrival_time
            std::vector<TripTime> rtable_ = std::vector<TripTime>();

        #if 0
            friend void Tempus::serialize( std::ostream& ostr, const PublicTransport::Timetable&, binary_serialization_t );
            friend void Tempus::unserialize( std::istream& istr, PublicTransport::Timetable&, binary_serialization_t );
        #endif
    };

    struct TimeFrequency
    {
        db_id_t pt_trip_id;

        ///
        /// Service start time, in seconds from the origin
        int32_t start_time;

        ///
        /// Service end time, in seconds from the origin
        int32_t end_time;

        ///
        /// Headway, number of seconds between each trip
        int32_t headway;
    };


    using ArcCost = nonstd::variant<std::map<db_id_t, GeneralizedCost>, Timetable, TimeFrequency>;
    ///
    /// Used as Directed Arc.
    /// Refers to the 'arc' DB's table
    struct Arc : public Base 
    {
        enum ArcType
        {
           RoadArc,
           PublicTransportArc
        };
        DECLARE_RW_PROPERTY( type, ArcType );

        /// [RoadArc] Allowed traffic rules. Bitfield. 
        DECLARE_RW_PROPERTY( traffic_rules, int32_t );

        ArcCost cost;

        public:
            Arc() : traffic_rules_(0) {}
    };

    // temporary type
    typedef boost::compressed_sparse_row_graph<boost::bidirectionalS, Node, Arc, /* GraphProperty */ boost::no_property, VertexIndexType, ArcIndexType> CSRGraph;

    ///
    /// The final road graph type
    class Graph
        : public CSRGraph
        , public RoutingData
    {
    public:
        typedef CSRGraph::vertex_descriptor node_descriptor;
        typedef CSRGraph::edge_descriptor arc_descriptor;

        typedef boost::graph_traits<CSRGraph>::vertex_iterator node_iterator;
        typedef boost::graph_traits<CSRGraph>::edge_iterator arc_iterator;
        typedef boost::graph_traits<CSRGraph>::out_edge_iterator out_arc_iterator;
        typedef boost::graph_traits<CSRGraph>::in_edge_iterator in_arc_iterator;

        Graph();

        typedef std::map<std::pair<Graph::node_descriptor, Graph::node_descriptor>, Arc> ArcMap;
        typedef std::vector<Node> NodeVector;
        
        Graph( const ArcMap& arcs, const NodeVector& nodes );

        virtual ~Graph() {}
        
        ///
        /// @return a node_descriptor from it's database id in O(1)
        Optional<node_descriptor> node_from_id( db_id_t id ) const;
        
        ///
        /// @return a arc_descriptor from it's database id in O(1)
        Optional<arc_descriptor> arc_from_id( db_id_t id ) const;
        
        /**
         * Sets a static cost for a given arc and transport mode.
         * "Static" means it does not depend on time.
         *
         * The generalized cost is split into two components:
         * - a travel time (in seconds)
         * - a fare
         *
         */
        void set_static_road_section_cost( arc_descriptor arc, db_id_t transport_mode_id, int32_t time, float fare );
        
        /**
         * Returns the static generalized cost associated to an arc and a transport mode.
         */
         
        /* PT travel time management */
        void set_pt_timetable( arc_descriptor arc, db_id_t mode_id, const std::vector<Timetable::TripTime>& timetable );
        void clear_pt_timetable();
        void add_to_pt_timetable( arc_descriptor arc, Timetable::TripTime trip_time );
        void sort_pt_timetable();
        void set_pt_frequency( arc_descriptor arc, TimeFrequency f );
        DECLARE_RW_PROPERTY( time_origin, Optional<DateTime> );
        
        /* Automaton management */
        void clear_restrictions();
        void add_restriction( Restriction restriction );
        void reset_restriction_automaton();
        const ArcRestrictionAutomaton& restriction_automaton() const { return automaton_; }
        const ArcRestrictionAutomaton& reverse_restriction_automaton() const { return reverse_automaton_; }
        
        /* Vehicles and parking management */    
        void add_parking_cost ( node_descriptor node, db_id_t vehicle_parking_rule, GeneralizedCost leaving_cost, GeneralizedCost taking_cost );    
        void clear_parking_costs();
        void add_vehicle ( node_descriptor node, db_id_t transport_mode_id );
        void clear_vehicles();         
        const std::unordered_map< node_descriptor, std::unordered_map<db_id_t, GeneralizedCost> >& vehicle_leaving_costs() const { return vehicle_leaving_costs_; }
        const std::unordered_map< node_descriptor, std::unordered_map<db_id_t, GeneralizedCost> >& vehicle_taking_costs() const { return vehicle_taking_costs_; }
        
        /* Mode changing costs management */    
        bool vehicle( const node_descriptor& node, db_id_t transport_mode_id ) const;
        GeneralizedCost vehicle_taking_cost( const node_descriptor& node, db_id_t vehicle_parking_rule ) const;
        GeneralizedCost vehicle_leaving_cost( const node_descriptor& node, db_id_t vehicle_parking_rule ) const;
        GeneralizedCost compute_forward_mode_changing_cost( const node_descriptor& node, TransportMode ini_mode, const TransportMode& fin_mode ) const;
        GeneralizedCost compute_backward_mode_changing_cost( const node_descriptor& node, TransportMode ini_mode, const TransportMode& fin_mode ) const; 
        const std::unordered_multimap< node_descriptor, db_id_t >& vehicles() const { return vehicles_; }
        
        void clear_pt_boarding_security_times();
        void update_pt_boarding_security_time( node_descriptor node, int32_t pt_boarding_security_time );
        
    private:
        ///
        /// Vertex map. Resetted when the road graph is changed
        std::unordered_map<db_id_t, node_descriptor> node_map_; 

        ///
        /// Arc map. Resetted when the road graph is changed
        std::unordered_map<db_id_t, arc_descriptor> arc_map_; 

        std::list<Restriction> restrictions_; 
        std::list<Restriction> reverse_restrictions_; 

        // Automaton for the forward direction
        ArcRestrictionAutomaton automaton_; 
        // Automaton for the backward direction
        ArcRestrictionAutomaton reverse_automaton_;
        
        std::unordered_map< node_descriptor, std::unordered_map<db_id_t, GeneralizedCost> > vehicle_leaving_costs_;
        std::unordered_map< node_descriptor, std::unordered_map<db_id_t, GeneralizedCost> > vehicle_taking_costs_;
        std::unordered_multimap< node_descriptor, db_id_t > vehicles_;
    };
    
    //void serialize( std::ostream& ostr, const Graph&, binary_serialization_t );
    //void unserialize( std::istream& istr, Graph&, binary_serialization_t );

    /* Travel time management */
    GeneralizedCost static_road_section_cost( const Arc& arc, db_id_t mode_id ); 
    Optional<Timetable::TripTime> next_departure( const Arc& arc, int32_t traffic_rule, int32_t time, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ); 
    Optional<Timetable::TripTime> previous_arrival( const Arc& arc, int32_t traffic_rule, int32_t time, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ); 
    GeneralizedCost compute_forward_arc_cost( const Arc& arc, const TransportMode& mode, int32_t time_point, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ); 
    GeneralizedCost compute_backward_arc_cost( const Arc& arc, const TransportMode& mode, int32_t time_point, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ); 
    
} // Tempus namespace

namespace boost {
    namespace detail {
        std::ostream& operator<<( std::ostream& ostr, const Tempus::Graph::arc_descriptor& e );
    }
}

#endif

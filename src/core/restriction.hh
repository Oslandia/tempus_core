/**
 *   Copyright (C) 2019 Oslandia <infos@oslandia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/// Automaton representing cost penalties on arc sequences
///
/// It is represented by a graph
/// Each node represents a automaton's state, each arc represents a possible transition.
/// A transition from a state s to a state s' is possible only in the presence of a given
/// road/multimodal node or arc, called a 'symbol'
///
/// The 'output' part of the automaton serves to represent penalties of road sequences
///
/// The automaton is build upon sequences of forbidden movements and penalties
/// The optimized graph (finite state machine) is constructed by following the Aho & Corasick algorithms

#ifndef TEMPUS_RESTRICTION_HH
#define TEMPUS_RESTRICTION_HH

//#include <boost/graph/graphviz.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "graph.hh"
#include "cost.hh"
#include "io.hh"

namespace Tempus {

///
/// A Restriction is a sequence of arcs associated with a (generalized) cost penalty for each mode
struct Restriction {

    typedef std::vector<db_id_t> ArcIdSequence;

    ///
    /// The arc sequence
    ArcIdSequence sequence;

    ///
    /// The time penalty associated to this sequence, for each set of traffic rules
    std::map<int32_t, int32_t> time_penalty;
    /// The fare penalty associated to this sequence, for each set of toll rules
    std::map<int32_t, float> fare_penalty;
};

template <class Symbol>	
class RestrictionAutomaton {
public:	
    
    struct Node {
        std::map<int32_t, int32_t> time_penalty_per_traffic_rules;
        std::map<int32_t, float> fare_penalty_per_toll_rules;
    };
    
    struct Arc {
        // Symbol (node or arc of the main graph) associated with the automaton arc (or transition)
        Symbol symbol;
    };
    
    typedef boost::adjacency_list< boost::vecS,
                                   boost::vecS,
                                   boost::bidirectionalS,
                                   Node,
                                   Arc > Graph;
    
    /// Node of the graph, i.e. state of the automaton
    typedef typename Graph::vertex_descriptor State;
    /// Arc of the graph, i.e. transition
    typedef typename Graph::edge_descriptor Transition;
    
    // Attributes 	
    Graph automaton_graph_;
    State initial_state_; 
    
    // Constructor
    RestrictionAutomaton()
    {
        automaton_graph_.clear(); 			
        initial_state_ = boost::add_vertex( automaton_graph_ ); 
    }
    
    // Methods
    void build( const std::list<Restriction>& sequences )
    {
        automaton_graph_.clear(); 
        
        initial_state_ = boost::add_vertex( automaton_graph_ ); 
        
        for ( const auto& r : sequences ) {
            add_sequence( r.sequence, r.time_penalty, r.fare_penalty );
        } 
	
        // The automaton graph is modified to represent the "next" function, the "failure" function is used to operate this transformation. 
        build_shortcuts( build_failure_function() );
    }
    
    ///
    /// Corresponds to the enter() function in Aho & al. paper, responsible for the creation of the "goto" function
    /// here represented as an automaton
    void add_sequence( const std::vector< Symbol >& symbol_sequence, const std::map<int32_t, int32_t>& time_penalty_per_traffic_rules, const std::map<int32_t, float>& fare_penalty_per_toll_rules )
    {
        State current_state = initial_state_ ;

        // Iterating over forbidden road sequences 
        for ( size_t j = 0; j < symbol_sequence.size(); j++ ) {
            std::pair< State, bool > transition_result = find_transition( current_state, symbol_sequence[j] );
				
            if ( transition_result.second == true )  {// this state already exists
                current_state = transition_result.first;
            }
            else { // this state needs to be inserted
                State s = boost::add_vertex( automaton_graph_ );
                
                add_transition_( current_state, s, symbol_sequence[j], automaton_graph_ );
                
                if ( j == symbol_sequence.size()-1 ) {
                    automaton_graph_[ s ].time_penalty_per_traffic_rules = time_penalty_per_traffic_rules;
                    automaton_graph_[ s ].fare_penalty_per_toll_rules = fare_penalty_per_toll_rules;
                }
                current_state = s; 
            } 
        }
    } 

    /// Build the failure function (see Aho & al.)
    std::map < State, State > build_failure_function()
    {
        COUT2 << "[automaton] building failure function" << std::endl;
        std::map< State, State > failure_function_; 
        failure_function_.clear(); 
        std::list< State > q; 
				
        // All successors of the initial state return to initial state when failing
        BGL_FORALL_OUTEDGES_T( initial_state_, arc, automaton_graph_, Graph ) {
            const State& s = target( arc, automaton_graph_ );
            q.push_back( s );
            failure_function_[ s ] = initial_state_;
        } 
				
        // Iterating over the list of states which do not have failure state
        State r, state;
        while ( ! q.empty() ) {
            r = q.front();
            q.pop_front();
            BGL_FORALL_OUTEDGES_T( r, arc, automaton_graph_, Graph ) {
                State s = target( arc, automaton_graph_ );
                Symbol a = automaton_graph_[arc].symbol;
                q.push_back( s );
                state = failure_function_[ s ];
                while ( find_transition( state, a ).second == false && state != initial_state_ ) {
                    state = failure_function_[ state ];
                }
                std::pair<State,bool> t = find_transition( state, a );
                if ( t.second == true ) {
                    failure_function_[ s ] = t.first;
                }
                else {
                    failure_function_[ s ] = initial_state_;
                }
            } 
        }
        COUT2 << "[automaton] end of failure function building" << std::endl;

        return failure_function_;
    }

    /// corresponds to building the "next" function
    void build_shortcuts( std::map < State, State > failure_function_)
    {
        using namespace boost;

        // copy vertices
        Graph automaton_copy;
        BGL_FORALL_VERTICES_T( v, automaton_graph_, Graph ) {
            State s = boost::add_vertex(automaton_copy);
            automaton_copy[s].time_penalty_per_traffic_rules = automaton_graph_[v].time_penalty_per_traffic_rules;
            automaton_copy[s].fare_penalty_per_toll_rules = automaton_graph_[v].fare_penalty_per_toll_rules;
        }

        std::list< State > q;

        BGL_FORALL_OUTEDGES_T( initial_state_, arc, automaton_graph_, Graph ) {
            TEMPUS_ASSERT( arc.get_property() );
            State s = target( arc, automaton_graph_ );
            Symbol a = automaton_graph_[arc].symbol;

            q.push_back( s );
            add_transition_( initial_state_, s, a, automaton_copy );
        }

        COUT2 << "[automaton] building shortcuts" << std::endl;
        std::map<State, State> next;
        State r; 
        while ( ! q.empty() ) {
            r = q.front();
            q.pop_front();

            State f = initial_state_;
            typename std::map<State,State>::const_iterator fit = failure_function_.find(r);
            TEMPUS_ASSERT( fit != failure_function_.end() );
            f = fit->second;
            if ( f != initial_state_ ) {
                TEMPUS_ASSERT( f != r ); // f != r, unless the failure function is not built correctly

                BGL_FORALL_OUTEDGES_T( f, arc, automaton_graph_, Graph ) {
                    State s = target( arc, automaton_graph_ );
                    Symbol a = automaton_graph_[arc].symbol;

                    // we add an arc from r to s in the copy
                    // warning: it only works because vertices are stored as indices, not addresses (vecS)
                    add_transition_( r, s, a, automaton_copy );
                }
            }
            BGL_FORALL_OUTEDGES_T( r, arc, automaton_graph_, Graph ) {
                State s = target( arc, automaton_graph_ );
                Symbol a = automaton_graph_[arc].symbol;

                q.push_back( target( arc, automaton_graph_ ) );
                add_transition_( r, s, a, automaton_copy );
            }
        }
        // the new automaton is ready, replace
        automaton_graph_ = automaton_copy;
        COUT2 << "[automaton] end of shortcut building" << std::endl;
    }
    
    ///
    /// Look for a transition in the automaton from the state q
    /// and involving an symbol (node or arc) e
    std::pair< State, bool > find_transition( State q, Symbol e ) const
    {
        BGL_FORALL_OUTEDGES_T( q, arc, automaton_graph_, Graph ) {
            if ( automaton_graph_[ arc ].symbol == e ) {
                return std::make_pair( target( arc, automaton_graph_ ), true );
            }
        }
        // If transition not found
        return std::make_pair( 0, false );
    }
    
    Optional<GeneralizedCost> penalty( State s, const TransportMode& mode ) const
    {
        GeneralizedCost cost;
        bool found=false;
        cost.time = 0;
        cost.fare = 0;
        
        const std::map<int32_t, int32_t>& time = automaton_graph_[s].time_penalty_per_traffic_rules;
        for (auto it = time.begin() ; it != time.end() ; it++ ) {
            if ( it->first & mode.traffic_rule() ) {
                COUT2 << "Traffic rule "<< mode.traffic_rule() << " having penalty " << it->second << std::endl; 
                cost.time = it->second;
                found = true;
                continue; 
            }
        }
        
        const std::map<int32_t, float>& fare = automaton_graph_[s].fare_penalty_per_toll_rules;
        for (auto it = fare.begin() ; it != fare.end() ; it++ ) {
            if ( it->first & mode.toll_rule() ) {
                COUT2 << "Toll rule "<< mode.toll_rule() << " having penalty " << it->second << std::endl; 
                cost.fare = it->second;
                found = true;
                continue; 
            }
        }
        
        if (found) {
            return cost;
        }
        else return {};
    }
    
private:
    // helper function
    void add_transition_( State r, State s, Symbol a, Graph& graph )
    {
        Transition e;
        bool ok;
        tie(e, ok) = boost::add_edge( r, s, graph );
        TEMPUS_ASSERT( ok );
        graph[e].symbol = a;
    }
};

///
/// A restriction automation on arc IDs
using ArcRestrictionAutomaton = RestrictionAutomaton<db_id_t>;

} // namespace Tempus

#endif

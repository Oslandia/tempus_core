/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2016-2019 CEREMA (http://www.cerema.fr)
 *   Copyright (C) 2012-2019 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "graph.hh"

using nonstd::get_if;

namespace Tempus
{
    // Node functions
    void Node::add_vehicle_parking_rule( db_id_t rule )
    {
        this->vehicle_parking_rules_ |= 1 << (rule-1);
    }

    void Node::clear_vehicle_parking_rules( )
    {
        this->vehicle_parking_rules_=0; 
    }
    
    // Timetable functions
    void Timetable::add( Timetable::TripTime tt )
    {
        table_.push_back( tt );
    }

    void Timetable::sort()
    {
        // copy the table into the reverse table
        rtable_ = table_;

        // sort table_ by "ascending" departure time
        std::sort( table_.begin(), table_.end(),
                   []( const TripTime& t1, const TripTime& t2 ) {
                       return t1.departure_time() < t2.departure_time();
                   } );
        // sort rtable by "descending" arrival time
        std::sort( rtable_.begin(), rtable_.end(),
                   []( const TripTime& t1, const TripTime& t2 ) {
                       return t1.arrival_time() > t2.arrival_time();
                   } );
    }

    Optional<Timetable::TripTime> Timetable::next_departure( int32_t traffic_rule, int32_t time_s, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time  ) const
    {
        // Returns an iterator pointing to the first element in the range [first, last) that is not less than
        // (i.e. greater or equal to) value, or last if no such element is found. 
        auto it = std::lower_bound( table_.begin(), 
                                    table_.end(), 
                                    time_s,
                                    []( const Timetable::TripTime& t1, int32_t t ) { return t1.departure_time() < t; }
                                  );
        for ( auto it2 = it; it2 != table_.end(); it2++ ) {
            if ( it2->traffic_rules() & traffic_rule ) {
                if ( 
                     ( ( pt_trip_id ) && ( *pt_trip_id == it2->pt_trip_id() ) ) || 
                     ( ( ( ( pt_trip_id ) && ( *pt_trip_id != it2->pt_trip_id() ) ) || ( !pt_trip_id ) ) && ( time_s + pt_boarding_security_time <= it2->departure_time() ) ) 
                   ) {
                    COUT2 << " => Departure: " << it2->departure_time() << ", arrival: " << it2->arrival_time() << ", trip ID: "<< it2->pt_trip_id() << ", traffic rules: " << it2->traffic_rules() << std::endl;
                    return *it2;
                }
            }
        }
        return {};
    }
    
    Optional<Timetable::TripTime> Timetable::previous_arrival( int32_t traffic_rule, int32_t time_s, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ) const
    {
        // Returns an iterator pointing to the first element in the range [first, last) that is not less than
        // (i.e. greater or equal to) value, or last if no such element is found. 
        auto it = std::lower_bound( rtable_.begin(), 
                                    rtable_.end(), 
                                    time_s,
                                    []( const Timetable::TripTime& t1, int32_t t ) { return t1.arrival_time() > t; }
                                  );
        for ( auto it2 = it; it2 != table_.end(); it2++ ) {
            if ( it2->traffic_rules() & traffic_rule ) {
                if ( 
                     ( !pt_trip_id ) || 
                     ( ( pt_trip_id ) && ( *pt_trip_id == it2->pt_trip_id() ) ) || 
                     ( ( ( pt_trip_id ) && ( *pt_trip_id != it2->pt_trip_id() ) ) && ( time_s - pt_boarding_security_time >= it2->arrival_time() ) ) 
                   ) {
                    COUT2 << " => Departure: " << it2->departure_time() << ", arrival: " << it2->arrival_time() << ", trip ID: "<< it2->pt_trip_id() << ", traffic rules: " << it2->traffic_rules() << std::endl;
                    return *it2;
                }
            }
        }
        return {};
    }
    
    // Graph functions
    Graph::Graph()
        : CSRGraph()
        , RoutingData( "multimodal_graph" )
    {}

    static const std::pair<Graph::node_descriptor, Graph::node_descriptor>& get_arc_key( const Graph::ArcMap::const_iterator::value_type& p )
    {
        return p.first;
    }

    Graph::Graph( const ArcMap& arcs, const NodeVector& nodes )
        : CSRGraph( boost::edges_are_unsorted_multi_pass,
                    boost::make_transform_iterator( arcs.begin(), get_arc_key ),
                    boost::make_transform_iterator( arcs.end(), get_arc_key ),
                    nodes.size() )
        , RoutingData( "multimodal_graph" )
    {
        COUT2 << "Assigning nodes ..." << std::endl;
        {
            size_t i = 0;
            for ( const Node& n : nodes ) {
                (*this)[i] = n;
                node_map_[n.db_id()] = i;
                i++;
            }
        }

        COUT2 << "Assigning arcs ..." << std::endl;
        for ( auto sit : arcs ) {
            Graph::arc_descriptor e;
            bool found;
            std::pair<Graph::node_descriptor, Graph::node_descriptor> p = sit.first;
            boost::tie(e, found) = edge( p.first, p.second, *this );
            TEMPUS_ASSERT( found );
            (*this)[e] = sit.second;
            arc_map_[sit.second.db_id()] = e;
        }
    }

    Optional<Graph::node_descriptor> Graph::node_from_id( db_id_t id ) const
    {
        auto it = node_map_.find( id );
        if ( it != node_map_.end() ) {
            return it->second;
        }
        return {};
    }

    Optional<Graph::arc_descriptor> Graph::arc_from_id( db_id_t id ) const
    {
        auto it = arc_map_.find( id );
        if ( it != arc_map_.end() ) {
            return it->second;
        }
        return {};
    }

    void Graph::set_static_road_section_cost( arc_descriptor arc, db_id_t transport_mode_id, int32_t time, float fare )
    {
        GeneralizedCost g;
        g.time = time;
        g.fare = fare;
        if ( auto r = get_if< std::map<db_id_t, GeneralizedCost> >( &(*this)[arc].cost ) ) {
            (*r)[transport_mode_id] = g;
        }
        else
        {
            std::map<db_id_t, GeneralizedCost> c;
            c[transport_mode_id] = g;
            (*this)[arc].cost = std::move( c );
        }
    }
    
    void Graph::clear_pt_timetable()
    {
        Graph::arc_iterator eit, eit_end;
        for ( std::tie( eit, eit_end ) = edges( *this ); eit != eit_end; eit++ ) {
            if ( auto r = get_if<Timetable>( &(*this)[*eit].cost ) ) {
                *r = Timetable();
            }
        }
    }

    void Graph::add_to_pt_timetable( arc_descriptor arc, Timetable::TripTime trip_time )
    {
        if ( auto r = get_if<Timetable>( &(*this)[arc].cost ) ) {
            r->add( trip_time );
        }
        else
        {
            Timetable tt;
            tt.add( trip_time );
            (*this)[arc].cost = std::move( tt );
        }
    }

    void Graph::sort_pt_timetable()
    {
        Graph::arc_iterator eit, eit_end;
        for ( std::tie( eit, eit_end ) = edges( *this ); eit != eit_end; eit++ ) {
            if ( auto r = get_if<Timetable>( &(*this)[*eit].cost ) ) {
                r->sort();
            }
        }
    }

    void Graph::set_pt_frequency( Graph::arc_descriptor arc, TimeFrequency freq )
    {
        (*this)[arc].cost = std::move( freq );
    }
    
    bool Graph::vehicle( const Graph::node_descriptor& node, db_id_t transport_mode_id ) const
    {
        bool veh_available = false;    
        auto veh_it = vehicles().equal_range( node );
        for (auto it = veh_it.first; it != veh_it.second; it++) {   
            COUT2 << "Vehicle of mode " << it->second << std::endl;
            if (it->second == transport_mode_id ) {
                veh_available = true;
                break;
            }
        }
        return veh_available;
    }

    GeneralizedCost Graph::vehicle_taking_cost( const Graph::node_descriptor& node, db_id_t vehicle_parking_rule ) const
    {  
        GeneralizedCost c;
        auto p_it = vehicle_taking_costs().find( node );
        if ( p_it != ( vehicle_taking_costs().end() ) ) {
            auto q_it = (p_it->second).find( vehicle_parking_rule );
            if ( q_it != (p_it->second).end() ) {
                c = q_it->second ;
                COUT2 << "  Vehicle taking penalty: time = " << c.time << ", additional cost = " << c.fare << std::endl;
            }
        }
        return c;
    }

    GeneralizedCost Graph::vehicle_leaving_cost( const Graph::node_descriptor& node, db_id_t vehicle_parking_rule ) const
    {  
        GeneralizedCost c;
        auto p_it = vehicle_leaving_costs().find( node );
        if ( p_it != ( vehicle_leaving_costs().end() ) ) {
            auto q_it = (p_it->second).find( vehicle_parking_rule );
            if ( q_it != (p_it->second).end() ) {
                c = q_it->second ;
                COUT2 << "  Vehicle leaving penalty: time = " << c.time << ", additional cost = " << c.fare << std::endl;
            }
        }
        return c;
    }

    GeneralizedCost Graph::compute_forward_mode_changing_cost( const node_descriptor& node , TransportMode ini_mode, const TransportMode& fin_mode ) const
    {
        GeneralizedCost c1, c2;
        c1.time = std::numeric_limits<int32_t>::max();
        c2.time = std::numeric_limits<int32_t>::max();
        c1.fare = 0;
        c2.fare = 0;
        
        if ( ( fin_mode.category() == TransportMode::Car ) || ( fin_mode.category() == TransportMode::Bicycle ) ) {
            if ( ( (*this)[node].vehicle_parking_rules() & fin_mode.vehicle_parking_rule() ) && vehicle( node, fin_mode.db_id() ) ) {
                c1 = vehicle_taking_cost( node, fin_mode.vehicle_parking_rule() );
            }
        }
        else c1.time = 0; // Changing to walking modes is always allowed and has no taking cost
        
        if ( c1.time < std::numeric_limits<int32_t>::max() ) {
            if ( ( ini_mode.category() == TransportMode::Car ) || ( ini_mode.category() == TransportMode::Bicycle ) ) {
                if ( (*this)[node].vehicle_parking_rules() & ini_mode.vehicle_parking_rule() ) {
                    c2 = vehicle_leaving_cost( node, ini_mode.vehicle_parking_rule() );
                }
            }
            else c2.time = 0; // Changing from walking modes is always allowed and has no leaving cost
        }
        
        if ( c2.time < std::numeric_limits<int32_t>::max() ) {
            c2.time += c1.time;
            c2.fare += c1.fare;
        }
        return c2;
    }

    GeneralizedCost Graph::compute_backward_mode_changing_cost( const node_descriptor& node, TransportMode ini_mode, const TransportMode& fin_mode ) const
    {
        GeneralizedCost c1, c2;
        c1.time = std::numeric_limits<int32_t>::max();
        c2.time = std::numeric_limits<int32_t>::max();
        c1.fare = 0;
        c2.fare = 0;
        
        if ( ( ini_mode.category() == TransportMode::Car ) || ( ini_mode.category() == TransportMode::Bicycle ) ) {
            if ( ( ( *this)[node].vehicle_parking_rules() & ini_mode.vehicle_parking_rule() ) && vehicle( node, ini_mode.db_id() ) ) {
                c1 = vehicle_taking_cost( node, ini_mode.vehicle_parking_rule() );
            }
        }
        else c1.time = 0; // Changing to walking modes is always allowed and has no taking cost
        
        if ( c1.time < std::numeric_limits<int32_t>::max() ) {
            if ( ( fin_mode.category() == TransportMode::Car ) || ( fin_mode.category() == TransportMode::Bicycle ) ) {
                if ( (*this)[node].vehicle_parking_rules() & fin_mode.vehicle_parking_rule() ) {
                    c2 = vehicle_leaving_cost( node, fin_mode.vehicle_parking_rule() ); 
                }
            }
            else c2.time = 0;
        }
        
        if ( c2.time < std::numeric_limits<int32_t>::max() ) {
            c2.time += c1.time;
            c2.fare += c1.fare;
        }
        return c2;
    }

    void Graph::clear_restrictions()
    {
        restrictions_.clear();
        reverse_restrictions_.clear();
    }

    void Graph::add_restriction( Restriction restriction )
    {
        // revert the arc sequence for the reverse case
        std::vector<db_id_t> seq = restriction.sequence;
        std::reverse(seq.begin(), seq.end());
        Restriction r_restriction;
        r_restriction.sequence = std::move( seq );
        r_restriction.time_penalty = restriction.time_penalty;
        r_restriction.fare_penalty = restriction.fare_penalty;
        reverse_restrictions_.push_back( std::move( r_restriction ) );
        
        restrictions_.push_back( std::move( restriction ) );
    }

    void Graph::reset_restriction_automaton()
    {
        automaton_.build( restrictions_ );
        reverse_automaton_.build( reverse_restrictions_ );
        restrictions_.clear();
        reverse_restrictions_.clear();
    }

    void Graph::add_parking_cost( node_descriptor node, db_id_t vehicle_parking_rule, GeneralizedCost leaving_cost, GeneralizedCost taking_cost )
    {
        vehicle_taking_costs_[node][vehicle_parking_rule] = taking_cost;
        vehicle_leaving_costs_[node][vehicle_parking_rule] = leaving_cost;
        (*this)[node].add_vehicle_parking_rule(vehicle_parking_rule);
    }

    void Graph::clear_parking_costs()
    {
        vehicle_taking_costs_.clear();
        vehicle_leaving_costs_.clear();
        vehicles_.clear();
        BGL_FORALL_VERTICES_T( node, (*this), Graph ) {
            (*this)[node].clear_vehicle_parking_rules();
        }
    } 

    void Graph::add_vehicle( node_descriptor node, db_id_t transport_mode_id )
    {
        vehicles_.insert(std::make_pair(node, transport_mode_id));
    }

    void Graph::clear_vehicles()
    {
        vehicles_.clear();
        COUT2 << "Vehicles cleared" << std::endl;
    }
    
    void Graph::clear_pt_boarding_security_times()
    {
        BGL_FORALL_VERTICES_T( node, (*this), Graph ) {
            (*this)[node].set_pt_boarding_security_time ( 0 );
        }
    }
    
    void Graph::update_pt_boarding_security_time( node_descriptor node, int32_t pt_boarding_security_time )
    {
        (*this)[node].set_pt_boarding_security_time ( pt_boarding_security_time );
    }
    
    // Global functions
    GeneralizedCost static_road_section_cost( const Arc& arc, const TransportMode& mode )
    {
        GeneralizedCost cost;
        if ( auto r = get_if< std::map<db_id_t, GeneralizedCost> >( &arc.cost ) ) {
            auto mode_it = r->find( mode.db_id() );
            if ( mode_it != r->end() ) {
                cost = mode_it->second;            
                if (mode.category() == TransportMode::Walking ) cost.walking_time = cost.time; 
                else if (mode.category() == TransportMode::Bicycle ) { cost.bicycle_time = cost.time; }
                return cost;
            }
        }
        return {};
    }
    
    Optional<Timetable::TripTime> next_departure( const Arc& arc, int32_t traffic_rule, int32_t time, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time  )
    {
        if ( auto r = get_if<Timetable>( &arc.cost ) ) {
            return r->next_departure( traffic_rule, time, pt_trip_id, pt_boarding_security_time );
        }
        return {};
    }
    
    Optional<Timetable::TripTime> previous_arrival( const Arc& arc, int32_t traffic_rule, int32_t time, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time )
    {
        if ( auto r = get_if<Timetable>( &arc.cost ) ) {
            return r->previous_arrival( traffic_rule, time, pt_trip_id, pt_boarding_security_time );
        }
        return {};
    }
    
    GeneralizedCost compute_forward_arc_cost( const Arc& arc, const TransportMode& mode, int32_t time_point, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ) 
    {
        GeneralizedCost cost;
        cost.time = std::numeric_limits<int32_t>::max();
        cost.fare = 0;
        
        if ( arc.type() == Arc::RoadArc ) {
            cost = Tempus::static_road_section_cost( arc, mode ); 
        }
        else if ( arc.type() == Arc::PublicTransportArc ) {
            Optional<Timetable::TripTime> tt = Tempus::next_departure( arc, mode.traffic_rule(), time_point, pt_trip_id, pt_boarding_security_time );
            if ( tt ) {
                cost.time = tt->arrival_time() - time_point;
                cost.pt_time = tt->arrival_time() - tt->departure_time(); 
                cost.waiting_time = tt->departure_time() - time_point;
                cost.pt_trip_id = tt->pt_trip_id();
            }
        }
        return cost;
    }

    GeneralizedCost compute_backward_arc_cost( const Arc& arc, const TransportMode& mode, int32_t time_point, Optional<db_id_t> pt_trip_id, int32_t pt_boarding_security_time ) 
    {
        GeneralizedCost cost;
        cost.time = std::numeric_limits<int32_t>::max();
        cost.fare = 0;
        
        if ( arc.type() == Arc::RoadArc ) {
            cost = Tempus::static_road_section_cost( arc, mode ); 
            if (pt_trip_id) {
                cost.time += pt_boarding_security_time;
                cost.waiting_time += pt_boarding_security_time;
            }
        }
        else if ( arc.type() == Arc::PublicTransportArc ) {
            Optional<Timetable::TripTime> tt = Tempus::previous_arrival( arc, mode.traffic_rule(), time_point, pt_trip_id, pt_boarding_security_time );
            if ( tt ) {
                cost.time = time_point - tt->departure_time();
                cost.pt_time = tt->arrival_time() - tt->departure_time();
                cost.waiting_time = time_point - tt->arrival_time();
                cost.pt_trip_id = tt->pt_trip_id();
            }
        }
        return cost;
    }
}

namespace boost {

    namespace detail {
        std::ostream& operator<<( std::ostream& ostr, const Tempus::Graph::arc_descriptor& e )
        {
            ostr << "(" << e.src << "+" << e.idx << ")";
            return ostr;
        }
    }
}

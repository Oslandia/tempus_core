/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "request.hh"

namespace Tempus {

Request::Request( db_id_t start_id, db_id_t end_id )
    : starting_node_( start_id )
    , ending_nodes_( end_id )
{
}

void Request::add_ending_node( db_id_t node )
{
    ending_nodes_.push_back( node );
}

void Request::add_allowed_transport_mode( db_id_t m )
{
    // add a new mode, make sure the resulting vector stay sorted
    allowed_transport_modes_.push_back( m );
    std::sort( allowed_transport_modes_.begin(), allowed_transport_modes_.end() );
}

} // Tempus namespace

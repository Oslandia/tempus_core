/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

//
// This file contains declarations of transport modes
//

#ifndef TEMPUS_TRANSPORT_MODES_HH
#define TEMPUS_TRANSPORT_MODES_HH

#include <map>
#include <string>
#include "common.hh"
#include "optional.hh"

namespace Tempus
{

/// Bit field type, used for traffic rules or speed rules.
/// It is stored as an int32, so 32 different values are possible.
//typedef uint32_t BitField;

class TransportMode : public Base
{
public:
    ///
    /// Transport mode category
    enum Category
    {
       Walking = 1,
       Car,
       Bicycle
    };
    
    /// name
    DECLARE_RW_PROPERTY( name, std::string );
    
    /// category
    DECLARE_RW_PROPERTY( category, Category );
    
    /// Bitfield
    DECLARE_RW_PROPERTY( traffic_rule, uint32_t );
    
    /// Constant speed value for all arcs
    DECLARE_RW_PROPERTY( constant_speed, float );

    /// Bitfield. Only makes sense when is_public_transport() is false and constant_speed is null
    DECLARE_RW_PROPERTY( speed_rule, uint32_t );

    /// Bitfield. Only makes sense when is_public_transport() is false
    DECLARE_RW_PROPERTY( toll_rule, uint32_t );
    
    /// Bitfield. Only makes sense when is_public_transport() is false
    DECLARE_RW_PROPERTY( vehicle_parking_rule, uint32_t  );

private:
#if 0
    friend void Tempus::serialize( std::ostream& ostr, const TransportMode&, binary_serialization_t );
    friend void Tempus::unserialize( std::istream& istr, TransportMode&, binary_serialization_t );
#endif
};

enum CommonSpeedRuleId
{
   CarSpeedRule=1
};

}

#endif

/**
 *   Copyright (C) 2019 Hugo Mercier <hugo.mercier@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPUS_COST_HH
#define TEMPUS_COST_HH

#include "optional.hh"

namespace Tempus
{
    
    struct GeneralizedCost
    {
        ///
        /// Travel time, in seconds
        int32_t time = std::numeric_limits<int32_t>::max();
        
        ///
        /// Fare, in local currency
        float fare = std::numeric_limits<float>::max();
        
        ///
        /// Waiting time, in seconds
        int32_t waiting_time = 0;
        
        ///
        /// Walking time, in seconds
        int32_t walking_time = 0;
        
        ///
        /// Bicycle time, in seconds
        int32_t bicycle_time = 0;
        
        ///
        /// Bicycle time, in seconds
        int32_t pt_time = 0;
        
        ///
        /// Trip id
        Optional<db_id_t> pt_trip_id;
    };

    struct GeneralizedCostWeights {
        ///
        /// Weight of PT in-vehicle time
        float pt_time_weight = 1;
        
        ///
        /// Weight of PT waiting time
        float waiting_time_weight = 1;
        
        ///
        /// Weight of individual modes in-vehicle time
        float indiv_mode_time_weight = 1;
        
        ///
        /// Weight of fares (for both PT and tolls)
        float fare_weight = 0;
        
        ///
        /// Weight of a PT transfer
        float pt_transfer_weight = 0;
        
        ///
        /// Weight of a mode change
        float mode_change_weight = 0;
    };

    struct OptimizationConstraints {
        ///
        /// Maximum number of PT transfers
        Optional<int16_t> max_pt_transfers;
        
        ///
        /// Maximum number of mode changes
        Optional<int16_t> max_mode_changes;
        
        ///
        /// Maximum walking time
        Optional<int32_t> max_walking_time;
        
        ///
        /// Maximum bicycle time
        Optional<int32_t> max_bicycle_time;
    };

}

#endif

#!/bin/sh
if [ -z "$DBSTRING" ]; then
    DBSTRING="dbname=tempus_toy_graph port=5435"
fi
loadtempus -R -t reset -d "${DBSTRING}"
psql -d "${DBSTRING}" < toy_graph_roads.sql
loadtempus -t gtfs -s toy_graph_gtfs.zip -d "${DBSTRING}" --pt-network PT
# fix abscissa_road_section
psql -d "${DBSTRING}" -c "update tempus_gtfs.stops set abscissa_road_section = 1.0"
